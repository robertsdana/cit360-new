package week04;
import java.net.*;
import java.io.*;

public class httpExample {
    public static String getHttpContent(String string) {
        String content="";
        try {

            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            StringBuilder sBuilder = new StringBuilder();

            String line = null;

            while ((line = reader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }

            content = sBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static String formatContent(String string) {
        String[] temp = string.split("Network</th>");
        String content = temp[1].split("<td>Comments")[0];
        String piece1 = content.replaceAll("</td><td>", ":  ");
        String piece2 = piece1.replaceAll("</td>", "");
        String piece3 = piece2.replaceAll("<br>", "");
        String piece4 = piece3.replaceAll("<tr>", "");
        String piece5 = piece4.replaceAll("</tr>", "");
        String formatted = piece5.replaceAll("<td>", "");
        return formatted;
    }

    public static void main(String[] args) {
        System.out.println(formatContent(getHttpContent("http://whois.arin.net/rest/ip/8.8.8.8")));

    }
}