package Week05;

public class junitExample {
    public static Integer doAddition(int [] a) {
        Integer answer = a[0];
        try {
            for(int i = 1; i < a.length; i++) {
                answer += a[i];
            }
        }
        catch(Exception e) {
            System.out.println("Something went wrong");
            System.err.println(e);
        }
        return answer;
    }
    public static Integer doMultiplication(int [] a) {
        Integer answer = a[0];
        try {
            for(int i = 1; i < a.length; i++) {
                answer *= a[i];
            }
        }
        catch(Exception e) {
            System.out.println("Something went wrong");
            System.err.println(e);
        }
        return answer;
    }
    public static Integer doSubtraction(int [] a) {
        Integer answer = a[0];
        try {
            for(int i = 1; i < a.length; i++) {
                answer -= a[i];
            }
        }
        catch(Exception e) {
            System.out.println("Something went wrong");
            System.err.println(e);
        }
        return answer;
    }
    public static Integer doDivision(int [] a) {
        Integer answer = a[0];
        try {
            for(int i = 1; i < a.length; i++) {
                answer /= a[i];
            }
        }
        catch(Exception e) {
            System.out.println("Something went wrong");
            System.err.println(e);
        }
        return answer;
    }

    public static void main(String[] args) {
        int[] array = {1,2,3};
        System.out.println(doAddition(array));
        System.out.println(doMultiplication(array));
        System.out.println(doSubtraction(array));
        System.out.println(doDivision(array));
    }
}
