package week06;

import java.util.concurrent.ExecutionException;

public class Executing {

    public static void main(String[] args) {
        DoingSomething ds = new DoingSomething();
        try {
            ds.DoingSomething();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

}
