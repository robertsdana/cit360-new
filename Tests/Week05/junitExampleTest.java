package Week05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertThat;
import org.hamcrest.CoreMatchers;

class junitExampleTest {

    @Test
    void doAddition() {
            int[] AdditionArray = {3,4,5};
            int answer = junitExample.doAddition(AdditionArray);
            assertEquals(12, answer);
            assertArrayEquals(new int[]{3, 4, 5}, AdditionArray);
    }

    @Test
    void doMultiplication() {
        int[] MultiplicationArray = {3,4,5};
        int[] New = {3,4,5};
        int answer = junitExample.doMultiplication(MultiplicationArray);
        assertFalse(answer == 53 );
        assertNotNull(MultiplicationArray);
        assertSame(60, answer);
    }

    @Test
    void doSubtraction() {
        int[] SubtractionArray = {5, 4, 3};
        int answer = junitExample.doSubtraction(SubtractionArray);
        assertNotSame(new int[]{3, 4, 5}, SubtractionArray);
        assertTrue(answer == -2);
    }

    @Test
    void doDivision() {
        int[] DivisionArray = {21, 7};
        int[] NullDivisionArray = null;
        int answer = junitExample.doDivision(DivisionArray);
        assertNull(NullDivisionArray);
        assertThat(answer, CoreMatchers.equalTo(3));
    }
}